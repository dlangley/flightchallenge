//
//  Flight.swift
//  Flights
//
//  Created by Dwayne Langley on 5/10/18.
//  Copyright © 2018 Dwayne Langley. All rights reserved.
//

import Foundation

struct Flight: Codable {
    let name: String, iata: String, icao: String, coordinates: [Double], runways: [RunWay]
}

struct RunWay: Codable, Equatable, Hashable {
    let direction: String, distance: Int, surface: String
}

extension Flight { // Convenience Computed Property for Sourcing data
    static var table : [[Flight]] {
        var results = [[Flight]]()
        var unorganizedList = [Flight]()
        var runWays = [RunWay]()
        guard let filepath = Bundle.main.url(forResource: "flights", withExtension: "json") else {
            print("Wrong Path")
            return results
        }
        do {
            var data = try Data.init(contentsOf: filepath)
            unorganizedList = try JSONDecoder().decode([Flight].self, from: data)
        } catch {
            print("Error Thrown: \(error.localizedDescription)")
        }
        
        
        // Iterate over unorganized list to separate them into a sectioned datamodel (aka organize a matrix)
        runWays = getRunWays(of: unorganizedList)
        
        
        for runWay in runWays {
            var section = [Flight]()
            for flight in unorganizedList where flight.runways.contains(runWay) {
                section.append(flight)
            }
            print("\n NEW SECTION: \(section) \n")
            results.append(section)
        }
        
        
        return results
    }
    
    static func getRunWays(of flights: [Flight]) -> [RunWay] {
        var result = [RunWay]()
        
        for flight in flights {
            guard flight.runways.count == 1 else {
                for runway in flight.runways {
                    result.append(runway)
                }
                continue
            }
            result.append(flight.runways.first!)
        }
        
        return Array(Set(result)).sorted {$0.distance < $1.distance}
    }
}
