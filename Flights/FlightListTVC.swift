//
//  ViewController.swift
//  Flights
//
//  Created by Dwayne Langley on 5/10/18.
//  Copyright © 2018 Dwayne Langley. All rights reserved.
//

import UIKit

class FlightListTVC: UITableViewController {
    
    var dataModel : [[Flight]] = Flight.table {
        willSet {
            print("Setting dataModel to: \n\(newValue)")
        }
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return dataModel.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "flightCell") as! FlightCell
        
        cell.flight = dataModel[indexPath.section][indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let runway = dataModel[section].first?.runways.first!
        return String(describing: runway!)
    }
}

class FlightCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var coordinateLabel: UILabel!
    @IBOutlet var iataLabel: UILabel!
    @IBOutlet var icaoLabel: UILabel!
    
    var flight : Flight? {
        willSet {
            self.nameLabel.text = newValue?.name
            self.coordinateLabel.text = String(describing: newValue?.coordinates)
            self.iataLabel.text = newValue?.iata
            self.icaoLabel.text = newValue?.icao
        }
    }
}





