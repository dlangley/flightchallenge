//
//  Instructions.swift
//  Flights
//
//  Created by Dwayne Langley on 5/10/18.
//  Copyright © 2018 Dwayne Langley. All rights reserved.
//

/*
 Show a list of flights
 
 Layout Details:
 - Organize sections by runway,
 - Each cell contains all of the rest of the information.
 */

// Json File parsed in...
// Datamodel Filled...
// Added Section Headers to Separate the data...
// Addressing Duplicate Runway listings...
